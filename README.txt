Advanced Taxonomy Menu

This is a modification of the taxonomy_menu module which creates the menu from any number of single level vocabularies, which is useful in situations where each category shares the same subcategories.  Take for instance a heirarchy like clothing.  The levels might be as follows:

vocab 1: Autumn, Summer, Fall
vocab 2: men, women
vocab 3: shirts, pants, shoes
vocab 4: size 10, 12, 14
vocab 5: colour red, green yellow

To create this heirarchy as a fixed system using taxonomy would be tiresome and error prone.  Instead the adv_taxonomy_menu enables you to set the the vocabulary to use at each level of the menu and creates the menu system from your directions.  Furthermore you can create any number of such menus on the same site.   It produces normal html code which can be themed using any standard css styling.

See Drupal thread: http://drupal.org/node/269773

INSTALLATION

Install module in the usual way
Create at least two vocabularies with some terms in each
Go to admin/settings/adv_taxonomy_menu and create a menu system out of these vocabularies 
This will create a block which you can then configure at admin/build/block and it will show any nodes which have been given these terms.
You can create as many of these menus as you like out of as many vocabularies as you like

API hook_adv_taxonomy_menu

The adv_taxonomy_menu engine can be used by other modules to modify menu systems on the fly by invoking hook_adv_taxononmy_menu_sql_alter.  The hook function takes the same arguments and is a modification of taxonomy_select_nodes().

Also available is function adv_taxonomy_menu_node_breadcrumb($terms) that returns a adv_taxonomy_menu breadcrumb based on supplied terms

VIEWS

In order for Views to work correctly you need to select the adv_taxonomy_menu Node Id range filter which can be found in the Node group of filters.  This filter is supplied dynamically with the nids that are required to be displayed.  There is no need to use any other filter for an adv_taxonomy_menu View.