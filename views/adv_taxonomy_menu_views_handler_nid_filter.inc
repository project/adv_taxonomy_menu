<?php

/**
 * Simple filter to handle greater than/less than filters
 */
class adv_taxonomy_menu_views_handler_nid_filter extends views_handler_filter {

  function operators() {
    $operators = array(
      'IN' => array(
        'title' => t('IN'),
        'short' => t('IN'),
        'values' => 1,
      ),
      'NOT IN' => array(
        'title' => t('NOT IN'),
        'short' => t('NOT IN'),
        'values' => 1,
      ),
    );

    return $operators;
  }
  function value_form(&$form, &$form_state) { 
    $form['value'] = array(
	  '#type' => 'hidden',
      '#value' => 'Supplied node nid range'
	);
  }

  /**
   * Provide a list of all the numeric operators
   */
  function operator_options($which = 'title') {
    $options = array();
    foreach ($this->operators() as $id => $info) {
      $options[$id] = $info[$which];
    }

    return $options;
  }

  function operator_values($values = 1) {
    $options = array();
    foreach ($this->operators() as $id => $info) {
      if ($info['values'] == $values) {
        $options[] = $id;
      }
    }

    return $options;
  }

  function query() {
    $this->ensure_my_table();
    $field = "$this->table_alias.$this->real_field";
	if(is_array($this->value) && !empty($this->value)) {
	  $place_holders = array();
	  foreach($this->value as $value) {
	    $place_holders[] = '%d';
	  }
	  $place_holders = '('.implode(",", $place_holders).')';
	  $this->query->add_where($this->options['group'], "$field $this->operator $place_holders", $this->value);
	}
  }
}