<?php

/**
 * Implementation of hook_views_tables():
 * Create a handler for the node nid range
 */
function adv_taxonomy_menu_views_data() {

  //   [TABLE_NAME][COLUMN]
  $data['node']['nid_range'] = array(
    'title'       => t('Adv_taxonomy_menu Node ID Range'),
    'help'        => t('This filter is required for Views to work in the adv_taxonomy_menu module.'),
    'filter'      => array(
	  'field'	  => 'nid',
      'handler'   => 'adv_taxonomy_menu_views_handler_nid_filter',
    ),
  );

  return $data;
}


function adv_taxonomy_menu_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'adv_taxonomy_menu') .'/views',
    ),
    'handlers' => array(
      // field handlers
      'adv_taxonomy_menu_views_handler_nid_filter' => array(
        'parent' => 'views_handler_filter',
      ),
    )
  );
}

